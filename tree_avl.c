#include <stdlib.h>
#include <malloc.h>
#include <math.h>

struct node{
	int k;
	int bf;
	int c;
	struct node* left;
	struct node* right;
};


int height(struct node* p)
{
    return p!=NULL?p->c:0;
}

int bfactor(struct node* p)
{
    return height(p->right)-height(p->left);
}

void fix(struct node* p)
{
    int hl, hr;
    hl = height(p->left);
    hr = height(p->right);
    p->c = ((hl>hr)?hl:hr)+1;
}

struct node* rot_right(struct node* p)
{
    struct node* q = p->left;
    p->left=q->right;
    q->right=p;
    fix(p);
    fix(q);
    return q;
}

struct node* rot_left(struct node* q)
{
    struct node* p = q->right;
    q->right = p->left;
    p->left = q;
    fix(q);
    fix(p);
    return p;
}

struct node* balance(struct node* p)
{
    fix(p);
    if(bfactor(p) == 2){
	if(bfactor(p->right) < 0)
	    p->right = rot_right(p->right);
	return rot_left(p);
    }
    if(bfactor(p) == -2){
	if(bfactor(p->left) < 0)
	    p->left = rot_left(p->left);
	return rot_right(p);
    }
    return p;
}

struct node* insert(struct node* p, int k)
{
    if(!p){
	struct node* q;
	q=malloc(sizeof(struct node));
	q->k=k;
	q->bf=0;
	q->c=0;
	q->left=NULL;
	q->right=NULL;
	return q;
    }
    if(k<p->k)
	p->left = insert(p->left, k);
    else
	p->right = insert(p->right, k);
    return balance(p);
}

void print_tree(struct node *p)
{
    if(p!=NULL){
	
	print_tree(p->left);
	printf("%d ", p->k);
	print_tree(p->right);
    }
}

int main()
{
    int a, *k, i, sum=0,ro;
    float c, d=0;
    struct node* root=NULL;
    printf("Vvedite kol-vo sluchainih chisel: ");
    scanf("%d",&a);
    k=malloc(sizeof(int)*a);
    for(i=0;i<a;i++){
	k[i]=rand()%100;
	printf("%d ",k[i]);
    }

    printf("\n\n");   



    for (i=0;i<a;i++){
	printf(" %d\n", i);	
	root=insert(root, k[i]);
	
    }

    print_tree(root);
    printf("\n");

}
